﻿using UnityEngine;
using System.Collections;

public class CarCrash : MonoBehaviour {
	
	void OnCollisionEnter(Collision other){
		if(other.collider.tag == "Cube"){
			Camera.main.GetComponent<EndGame>().EndTheGame ();
			Destroy(this);
		}
	}
}
