﻿using UnityEngine;
using System.Collections;

public class PlayerSound : MonoBehaviour {

	public AudioSource jmpSnd;
	public AudioSource movSnd;
	private AudioSource[] audioSounds;

	// Use this for initialization
	void Start () {
		audioSounds = GetComponents<AudioSource> ();
		jmpSnd = audioSounds [1];
		movSnd = audioSounds [0];
		movSnd.volume = 0.5F;
		jmpSnd.volume = 0.5F;
		movSnd.Play ();

	}
	
	// Update is called once per frame
	void Update () {

		movSnd.pitch = Random.Range (1, 20);
	}



}
