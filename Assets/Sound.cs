﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {

	public AudioSource rumbleAud;
	public AudioSource argueAud;
	public AudioSource backgroundMus;
	public AudioSource gloomyMusic;
	public AudioSource[] audioSources;

	// Use this for initialization
	void Start () {
		audioSources = GetComponents<AudioSource>();
		argueAud = audioSources [1];
		rumbleAud = audioSources [0];
		backgroundMus = audioSources [2];
		argueAud.volume = 0.01F;
		rumbleAud.volume = 0.0F;
		backgroundMus.volume = 0.001F;
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.GetComponent<Look>().currentState.Equals(Look.GameState.Unfocused) && rumbleAud.volume > 0.0F) {
			rumbleAud.volume -= 0.0004F;
		} else if (gameObject.GetComponent<Look>().currentState.Equals(Look.GameState.Focused) && rumbleAud.volume < 0.7F){
			rumbleAud.volume += 0.0004F;
		}
		if (gameObject.GetComponent<Look>().currentState.Equals (Look.GameState.Unfocused) && argueAud.volume < 0.05F) {
			argueAud.volume += 0.001F;
		} else if (gameObject.GetComponent<Look>().currentState.Equals (Look.GameState.Focused) && argueAud.volume > 0.0F) {
			argueAud.volume -= 0.0008F;
		}
		if (gameObject.GetComponent<Look> ().currentState.Equals (Look.GameState.Unfocused) && backgroundMus.volume > 0.001F) {
						backgroundMus.volume -= 0.0018F;		
				} else if (gameObject.GetComponent<Look> ().currentState.Equals (Look.GameState.Focused) && backgroundMus.volume <= 0.1F) {
			backgroundMus.volume += 0.0004F;
				}

	}
}
