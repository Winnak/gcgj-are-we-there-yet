﻿using UnityEngine;

/// Dette er unity's char controller eksempel jeg har modificeret

/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)
public class Look : MonoBehaviour
{
    public enum GameState { Idle, Focused, Unfocused }

    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }

    public RotationAxes axes = RotationAxes.MouseXAndY;
    public GameState currentState;
    private float sensitivityX = 15F;
    private float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    private float rotationY = 0F;
    private float rotationX = 0F;

    //Custom
    private float fovChangeFactor = 0f;

    public bool useController = true;
    private const float ControllerSensMod = 0.05f;
    public bool isLeft = false;
    public GameObject runner;
    private float spawntimer;

    private Camera thisCamera;

    private void Update()
    {
        if (axes == RotationAxes.MouseXAndY)
        {
            if (this.useController)
            {
                rotationX = transform.localEulerAngles.y + Input.GetAxis("Horizontal") * sensitivityX * ControllerSensMod;

                rotationY += Input.GetAxis("Vertical") * sensitivityY * ControllerSensMod;
            }
            else
            {
                rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            }

            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
        }
        else
        {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }

        // Focus ud af vinduerne
        if (gameObject.transform.rotation.eulerAngles.y < 360 && gameObject.transform.rotation.eulerAngles.y > 320)
        {
            if (thisCamera.fieldOfView > 35)
            {
                currentState = GameState.Unfocused;
                this.fovChangeFactor += 0.02f * fovChangeFactor + Time.deltaTime;
                thisCamera.fieldOfView -= fovChangeFactor * Time.deltaTime;
            }
            else
            {
                currentState = GameState.Focused;
                isLeft = false;
            }
        }
        else if (gameObject.transform.rotation.eulerAngles.y < 210 && gameObject.transform.rotation.eulerAngles.y > 170)
        {
            if (thisCamera.fieldOfView > 35)
            {
                currentState = GameState.Unfocused;
                this.fovChangeFactor += 0.02f * fovChangeFactor + Time.deltaTime;
                thisCamera.fieldOfView -= fovChangeFactor * Time.deltaTime;
            }
            else
            {
                currentState = GameState.Focused;
                isLeft = true;
            }
        }
        else
        {
            currentState = GameState.Unfocused;
            if (thisCamera.fieldOfView < 95f)
            {
                thisCamera.fieldOfView += 20f * Time.deltaTime;
            }

            if (this.fovChangeFactor > 1)
            {
                this.fovChangeFactor -= 0.05f * fovChangeFactor + Time.deltaTime * Time.deltaTime;
            }

            isLeft = false;
        }

        // Fix sens
        sensitivityX = thisCamera.fieldOfView * 0.05f; // fov*max fov/max sens
        sensitivityY = sensitivityX; // fov*max fov/max sens

        if (JumpingMan.living == 0 && !GetComponent<EndGame>().stopEverything)
        {
            if (currentState == GameState.Focused)
            {
                spawntimer += Time.deltaTime;
                if (spawntimer > 2)
                {
                    if (isLeft)
                    {
                        Instantiate(runner, new Vector3(0.6f, 13.5f, -50f), Quaternion.identity);
                    }
                    else
                    {
                        Instantiate(runner, new Vector3(0.6f, 13.5f, 50f), Quaternion.identity);
                    }
                    spawntimer = 0;
                }
            }
        }
        if (Input.GetButtonDown("Controller"))
        {
            useController = !useController;
        }
    }

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        // Make the rigid body not change rotation
        Rigidbody rigidbody1 = GetComponent<Rigidbody>();
        if (rigidbody1)
            rigidbody1.freezeRotation = true;

        thisCamera = GetComponent<Camera>();
    }
}