﻿using UnityEngine;
using System.Collections;

public class AnimationTest : MonoBehaviour {

	public float verticalSpeed;
	private Animator animator;

	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		verticalSpeed = GetComponent<Rigidbody>().velocity.y;
		animator.SetFloat("VerticalSpeed", verticalSpeed);
		if(Input.GetButtonDown("Jump")){
			GetComponent<Rigidbody>().velocity = Vector3.up * 10;
			animator.SetBool("StartJump", true);
		} if (Input.GetButtonUp("Jump")){
			animator.SetBool("StartJump", false);
		}
	}
}
