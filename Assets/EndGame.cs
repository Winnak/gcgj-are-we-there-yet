﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour
{
	private float timerMax, timerTick;
	private AudioSource[] audioSources;
	private AudioSource deathSound;
	private AudioSource deathMusic;
	private int lives = 3;
	public bool stopEverything;
	public GameObject doomsDay;
	public AudioClip[] screams;
	public GameObject block;

	void OnDisable ()
	{
		RenderSettings.skybox.SetColor ("_Tint", new Color (0.671f, 0.729f, 0.741f, 0.5f));
	}

	// Use this for initialization
	void Start ()
	{
		audioSources = GetComponents<AudioSource> ();
		deathSound = audioSources [3];
		deathMusic = audioSources [4];
		timerMax = 120.0F;
		timerTick = 0.0F;
	}

	// Update is called once per frame
	void Update ()
	{
	
		if (gameObject.GetComponent<Look> ().currentState.Equals (Look.GameState.Unfocused)) {
			timerTick += Time.deltaTime * 0.1F;
			if (timerTick >= timerMax && !stopEverything) {
				timerTick = 0.0F;
				Win ();
			}
		} else if (gameObject.GetComponent<Look> ().currentState.Equals (Look.GameState.Focused)) {
			timerTick += Time.deltaTime;
			if (timerTick >= timerMax && !stopEverything) {
				timerTick = 0.0F;
				Win ();
			}
		}
	}

	void Lose ()
	{
		stopEverything = true;
		Instantiate (doomsDay, Vector3.zero, Quaternion.identity);
		Instantiate (block, new Vector3 (-800, 0, 0), Quaternion.identity);
	}

	void Win ()
	{
		stopEverything = false;
		GameObject.Find ("SceneWrap").GetComponent<FadeCam> ().fadeDir = 1;
		gameObject.GetComponent<Sound> ().backgroundMus.volume = 0.5F;

	}

	public void EndTheGame ()
	{
		deathSound.Play ();
		deathMusic.Play ();
		gameObject.GetComponent<Sound> ().backgroundMus.Stop ();
		gameObject.GetComponent<Sound> ().rumbleAud.clip = screams [1];
		gameObject.GetComponent<Sound> ().argueAud.clip = screams [0];
		gameObject.GetComponent<Sound> ().rumbleAud.Play ();
		gameObject.GetComponent<Sound> ().argueAud.Play ();
		gameObject.GetComponent<Sound> ().rumbleAud.loop = false;
		gameObject.GetComponent<Sound> ().argueAud.loop = false;
		GameObject.Find ("SceneWrap").GetComponent<FadeCam> ().fadeDir = 1;
	}

	public void Death ()
	{
		if (GetComponent<Look> ().currentState == Look.GameState.Focused) {
			lives--;
			if (lives < 1) {
				Lose ();
			}
		}
	}

}
