﻿using UnityEngine;
using System.Collections;

public class FadeCam : MonoBehaviour {

	private SpriteRenderer sprite;
	public int fadeDir = -1;
	private bool didOneLoad;

	// Use this for initialization
	void Start () {
		sprite = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(fadeDir != 0){
			Color newColor = sprite.GetComponent<Renderer>().material.color;
			newColor.a += Time.deltaTime * fadeDir * .5f;
			if(newColor.a < 0){
				newColor.a = 0;
			}
			if(newColor.a > 1){
				newColor.a = 1;
			}
			sprite.GetComponent<Renderer>().material.color = newColor;
		}
	}
	
	public void CheckBlockLoading(){
		if(!didOneLoad){
			didOneLoad = true;
		} else {
			fadeDir = -1;
		}
	}
}
