﻿using UnityEngine;
using System.Collections;
//FindObjectOfType<SkyboxStyles>().beginDoom = true;
public class SkyboxStyles : MonoBehaviour {
	private Color temp = new Color(0.671f, 0.729f, 0.741f, 0.5f);
	private Color temp2;
	private Color end = new Color(0.695f, 0.32f, 0.1f, 0.5f);
	//private Color end2 = new Color(0.675f, 0.62f, 0.6f, 0.5f);
	private float tid;
	// Use this for initialization
	void Start () {
		temp2 = RenderSettings.ambientLight;
	}

	void Update() {
		if (tid < 0.01f) {
			temp = Color.Lerp(temp, end, tid);
			temp2 = Color.Lerp(temp2, end, tid);
			tid += Time.deltaTime * 0.001f;
			RenderSettings.skybox.SetColor("_Tint", temp);
			RenderSettings.ambientLight = temp2;
		} else {
			Destroy(this.gameObject);
		}
	}
}
