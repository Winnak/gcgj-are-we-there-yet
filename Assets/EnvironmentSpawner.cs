﻿using UnityEngine;
using System.Collections;

public class EnvironmentSpawner : MonoBehaviour
{

	public float startX = 200;
	public Vector3 MaxSpeed;
	public Vector3 speed;
	public int blockMin = 10;
	public GameObject block;
	public int minBlockSize;
	public int maxBlockSize;
	public float slowSpeed = 0.1f;
	private float slowAccel = 0f;
	private GameObject lastSpawn;
	
	// Use this for initialization
	void Start ()
	{
		this.name = transform.position.z < 0 ? "Left" : "Right";
		this.name += " Environment Spawner";
		GameObject newLastSpawn = new GameObject ();
		lastSpawn = newLastSpawn;
		Vector3 newSpawn = transform.position;
		newSpawn.x = startX;
		newSpawn.y = -10;
		lastSpawn.transform.position = newSpawn;
		while (lastSpawn.transform.position.x > -startX) {
			GameObject newBlock = Instantiate (block, lastSpawn.transform.position, Quaternion.identity) as GameObject;
			SpawnNewBlock (newBlock);
			newBlock.transform.parent = this.transform;
		}
		Destroy (newLastSpawn);
		GameObject.Find ("SceneWrap").GetComponent<FadeCam> ().CheckBlockLoading ();
	}

	public void SpawnNewBlock (GameObject newBlock)
	{
		newBlock.transform.localScale = new Vector3(
			Random.Range(minBlockSize - minBlockSize / 2, maxBlockSize) * 0.2f, 
			Random.Range (minBlockSize, maxBlockSize) * 0.2f, 
			Random.Range (minBlockSize, maxBlockSize) * 0.05f);

		Vector3 newSpawn = transform.position;
		newSpawn.y = newBlock.transform.localScale.y / 2 - 2f;
		newSpawn.x = lastSpawn.transform.position.x - lastSpawn.transform.localScale.x / 2 - newBlock.transform.localScale.x / 2;
		newBlock.transform.position = newSpawn;
		lastSpawn = newBlock;
		if (!newBlock.GetComponent<Rigidbody> ().useGravity && Camera.main.GetComponent<EndGame> ().stopEverything) {
			newBlock.GetComponent<Rigidbody> ().useGravity = true;
			newBlock.GetComponent<Rigidbody> ().isKinematic = false;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (FindObjectOfType<Look> ().currentState == Look.GameState.Focused) {
			if (speed.x > MaxSpeed.x * slowSpeed + slowAccel) {
				speed.x -= MaxSpeed.x * 0.01f;
				if (slowSpeed + slowAccel < 1) {
					slowAccel += Time.deltaTime * 0.1f;
				}

				if (speed.x < MaxSpeed.x * slowSpeed + slowAccel) {
					speed.x = slowSpeed + slowAccel;
				}
			}

		} else {
			speed = MaxSpeed;
			slowAccel = 0;
		}
	}
}
