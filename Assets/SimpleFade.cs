﻿using UnityEngine;
using System.Collections;

public class SimpleFade : MonoBehaviour {

	private SpriteRenderer sprite;
	private float waitTime = 2;

	// Use this for initialization
	void Start () {
		sprite = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(waitTime > 0){
			waitTime -= Time.deltaTime;
		} else {
			Color newColor = sprite.GetComponent<Renderer>().material.color;
			newColor.a -= Time.deltaTime * 0.4f;
			if(newColor.a <= 0){
				gameObject.SetActive(false);
			} else {
				sprite.GetComponent<Renderer>().material.color = newColor;
			}
		}
	}
}
