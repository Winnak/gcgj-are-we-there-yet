﻿using UnityEngine;
using System.Collections;

public class EnvironmentComponentMover : MonoBehaviour {

	private EnvironmentSpawner spawner;

	void Start(){
		string sideOfRoad = transform.position.z < 0 ? "Left" : "Right";
		sideOfRoad += " Environment Spawner";
		spawner = GameObject.Find(sideOfRoad).GetComponent<EnvironmentSpawner>();
	}

	// Update is called once per frame
	void FixedUpdate () {
		GetComponent<Rigidbody>().MovePosition(transform.position + spawner.speed);
		if(transform.position.x > spawner.startX){
			spawner.SpawnNewBlock(this.gameObject);
		}
	}
}
