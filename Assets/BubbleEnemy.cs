﻿using UnityEngine;

public class BubbleEnemy : MonoBehaviour
{
    public float danger = 1;
    private float lifeTime = 20f;

    private void Start()
    {
        gameObject.transform.localScale = new Vector3(danger, danger, danger);
    }

    private void FixedUpdate()
    {
        GetComponent<Rigidbody>().MovePosition(transform.position - new Vector3(-0.1f * danger, 0, 0));
    }

    private void Update()
    {
        if (lifeTime < 0)
        {
            Destroy(gameObject);
        }
        else
        {
            lifeTime -= Time.deltaTime;
        }
    }
}