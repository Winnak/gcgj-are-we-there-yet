﻿using UnityEngine;
using System.Collections;

public class JumpingMan : MonoBehaviour
{
	public float distToGround;
	public float jumpSpeed;
	public bool jump;
	public int jumps;
	private bool prevMouseState;
	public static int living = 0;
	public float verticalSpeed;
	private Animator animator;
	public AudioSource jmpSnd;
	public GameObject enemyBubble;
	private float bubbleTimer;
	public float bubbleInterval = 5;

	void Start ()
	{
		animator = gameObject.GetComponent<Animator> ();
		living++;
		distToGround = GetComponent<Collider> ().bounds.extents.y;
		jumps = 2;
		jmpSnd = GetComponent<AudioSource>();
		jmpSnd.volume = 0.2F;
	}

	void Update ()
	{
		animator.SetFloat ("VerticalSpeed", GetComponent<Rigidbody> ().velocity.y);
		if (Input.GetButtonDown ("Jump") && jumps > 0) {
			animator.SetBool ("StartJump", true);
			jumps--;
			jmpSnd.Play ();
			GetComponent<Rigidbody> ().velocity = new Vector3 (0, jumpSpeed, 0);
		} else if (Input.GetButtonUp ("Jump")) {
			animator.SetBool ("StartJump", false);
		}

		//Reset jumps
		if (IsGrounded ()) {
			jumps = 2;
		}

		//Move player
		if (this.transform.position.x > 0.6f) {
			GetComponent<Rigidbody> ().velocity = new Vector3 (-5f, GetComponent<Rigidbody> ().velocity.y, 0);
		} else {
			GetComponent<Rigidbody> ().velocity = new Vector3 (0, GetComponent<Rigidbody> ().velocity.y, 0);
		}

		// Death conditions
		if (this.transform.position.y < 0) {
			Die ();
		}
		if (this.transform.position.x > 20.0f) {
			Die ();
		}
		if (bubbleTimer < bubbleInterval) {
			bubbleTimer += Time.deltaTime;
		} else {
			bubbleTimer = 0;
			bubbleInterval -= 0.1f;
			GameObject newBubble = Instantiate (enemyBubble, transform.position + new Vector3 (-30, Random.Range (-5, 5), 0), Quaternion.identity) as GameObject;
			newBubble.GetComponent<BubbleEnemy> ().danger = 6 - bubbleInterval;
		}
	}

	bool IsGrounded ()
	{
		bool isGrounded = Physics.Raycast (transform.position, Vector3.down, distToGround + 0.1f);
		animator.SetBool ("IsGrounded", isGrounded);

		return isGrounded;
	}
	
	private void Die ()
	{
		living--;
		Camera.main.GetComponent<EndGame> ().Death ();
		Destroy (gameObject);
	}
}
